package composite;

/* Hier niets veranderen!
*/

public interface Component {

  long getSize();

  String getPath();

  void setParent(Directory parent);
}